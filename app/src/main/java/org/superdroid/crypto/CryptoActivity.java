package org.superdroid.crypto;

import android.animation.*;
import android.app.*;
import android.content.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;
import android.widget.CompoundButton.*;
import java.io.*;
import java.util.*;

public class CryptoActivity extends Activity {
	public int s = Toast.LENGTH_LONG;
	public int k = 0;
	public String rb = "randomBasamagi";
	public String re = "randomEksi";
	public String gm = "geceModu";
	public String rm = "rastgeleModu";
	public String km = "kopyalamaModu";
	public String dy = "dosyayaYaz";
	public String ts = "temaSecimi";
	String tc = "tersCevir";
	public boolean btn1 = false;
	public boolean btn2 = false;
	public boolean btn3 = false;
	public boolean hata = false;
	public boolean geri = false;
	public boolean ucik = false;
	public boolean rand = false;
	public boolean nrnd = false;
	public boolean dsyz = false;
	
	View sb;
	Button bt1;
	Button bt2;
	Button bt3;
	Button bt4;
	Button ab1;
	Button ab2;
	Button ab3;
	Switch dsy;
	Switch gms;
	Switch rms;
	Switch rns;
	Switch ktc;
	TextView pb1;
	TextView pb2;
	TextView sb1;
	TextView sb2;
	TextView sb3;
	TextView abt;
	TextView hkb;
	TextView hkm;
	TextView rnd;
	TextView kms;
	TextView rba;
	TextView tsc;
	TextView tet1;
	TextView tet2;
	TextView tet3;
	EditText et1;
	EditText et2;
	EditText et3;
	RadioGroup rbs;
	RadioButton rnd1;
	RadioButton rnd2;
	RadioButton rnd3;
	RadioButton rnd4;
	RadioButton rnd5;
	RadioButton rnd6;
	RadioButton rnd7;
	RadioButton rnd8;
	RadioGroup tms;
	RadioButton tm1;
	RadioButton tm2;
	RadioButton tm3;
	RadioButton tm4;
	RadioButton tm5;
	RadioButton tm6;
	RadioButton tm7;
	RadioButton tm8;
	LinearLayout tb;
	LinearLayout hky;
	LinearLayout aym;
	RelativeLayout sv1;
	SharedPreferences.Editor se;
	SharedPreferences sp;
	
    @Override
    protected void onCreate(Bundle crypto){
        super.onCreate(crypto);
		sp = getSharedPreferences("ayarlar",Context.MODE_PRIVATE);
        setContentView(R.layout.crypto);
		sdk();
		sb = findViewById(R.id.sb);
		bt1 = (Button) findViewById(R.id.bt1);
		bt2 = (Button) findViewById(R.id.bt2);
		bt3 = (Button) findViewById(R.id.bt3);
		bt4 = (Button) findViewById(R.id.bt4);
		ab1 = (Button) findViewById(R.id.ab1);
		ab2 = (Button) findViewById(R.id.ab2);
		ab3 = (Button) findViewById(R.id.ab3);
		dsy = (Switch) findViewById(R.id.dsy);
		gms = (Switch) findViewById(R.id.gms);
		rms = (Switch) findViewById(R.id.rms);
		rns = (Switch) findViewById(R.id.rns);
		ktc = (Switch) findViewById(R.id.ktc);
		pb1 = (TextView) findViewById(R.id.pb1);
		pb2 = (TextView) findViewById(R.id.pb2);
		sb1 = (TextView) findViewById(R.id.sb1);
		sb2 = (TextView) findViewById(R.id.sb2);
		sb3 = (TextView) findViewById(R.id.sb3);
		abt = (TextView) findViewById(R.id.abt);
		hkb = (TextView) findViewById(R.id.hkb);
		hkm = (TextView) findViewById(R.id.hkm);
		rnd = (TextView) findViewById(R.id.rnd);
		kms = (TextView) findViewById(R.id.kms);
		rba = (TextView) findViewById(R.id.rba);
		tsc = (TextView) findViewById(R.id.tsc);
		tet1 = (TextView) findViewById(R.id.tet1);
		tet2 = (TextView) findViewById(R.id.tet2);
		tet3 = (TextView) findViewById(R.id.tet3);
		et1 = (EditText) findViewById(R.id.et1);
		et2 = (EditText) findViewById(R.id.et2);
		et3 = (EditText) findViewById(R.id.et3);
		rbs = (RadioGroup) findViewById(R.id.rbs);
		rnd1 = (RadioButton) findViewById(R.id.rnd1);
		rnd2 = (RadioButton) findViewById(R.id.rnd2);
		rnd3 = (RadioButton) findViewById(R.id.rnd3);
		rnd4 = (RadioButton) findViewById(R.id.rnd4);
		rnd5 = (RadioButton) findViewById(R.id.rnd5);
		rnd6 = (RadioButton) findViewById(R.id.rnd6);
		rnd7 = (RadioButton) findViewById(R.id.rnd7);
		rnd8 = (RadioButton) findViewById(R.id.rnd8);
		tms = (RadioGroup) findViewById(R.id.tms);
		tm1 = (RadioButton) findViewById(R.id.tm1);
		tm2 = (RadioButton) findViewById(R.id.tm2);
		tm3 = (RadioButton) findViewById(R.id.tm3);
		tm4 = (RadioButton) findViewById(R.id.tm4);
		tb = (LinearLayout) findViewById(R.id.tb);
		hky = (LinearLayout) findViewById(R.id.hb);
		aym = (LinearLayout) findViewById(R.id.ayarmenu);
		sv1 = (RelativeLayout) findViewById(R.id.sv);
		se = sp.edit();
		
		if(sp.getBoolean(gm,false)){
			gms.setChecked(true);
			geceModu(true);
		} else {
			gms.setChecked(false);
			geceModu(false);
		}
		
		bt1.setEnabled(false);
		bt2.setEnabled(false);
		bt3.setEnabled(false);
		et1.setLongClickable(false);
		et2.setLongClickable(false);
		et3.setLongClickable(false);
		
		if(sp.getBoolean(rm,false)) rand = true;
		else rand = false;
		
		if(sp.getBoolean(dy,false)) dsyz = true;
		else dsyz = false;
		
		if(sp.getBoolean(re,false)) nrnd = true;
		else nrnd = false;
		
		dsy.setChecked(dsyz);
		rms.setChecked(rand);
		rns.setChecked(nrnd);
		ktc.setChecked(sp.getBoolean(tc,false));
		eksiKDO(nrnd);
		
		if(sp.getInt(rb,0)>=0 && sp.getInt(rb,0)<8){
			if(sp.getInt(rb,0)==0) rnd1.setChecked(true);
			if(sp.getInt(rb,0)==1) rnd2.setChecked(true);
			if(sp.getInt(rb,0)==2) rnd3.setChecked(true);
			if(sp.getInt(rb,0)==3) rnd4.setChecked(true);
			if(sp.getInt(rb,0)==4) rnd5.setChecked(true);
			if(sp.getInt(rb,0)==5) rnd6.setChecked(true);
			if(sp.getInt(rb,0)==6) rnd7.setChecked(true);
			if(sp.getInt(rb,0)==7) rnd8.setChecked(true);
		} else {
			rnd1.setChecked(true);
			se.putInt(rb,0);
			se.commit();
		}
		
		if(sp.getInt(ts,0)>=0 && sp.getInt(ts,0)<8){
			if(sp.getInt(ts,0)==0){
				tm1.setChecked(true);
				temaDegistir(0,sp.getBoolean(gm,false));
			} if(sp.getInt(ts,0)==1){
				tm2.setChecked(true);
				temaDegistir(1,sp.getBoolean(gm,false));
			} if(sp.getInt(ts,0)==2){
				tm3.setChecked(true);
				temaDegistir(2,sp.getBoolean(gm,false));
			} if(sp.getInt(ts,0)==3){
				tm4.setChecked(true);
				temaDegistir(3,sp.getBoolean(gm,false));
			}
		} else {
			tm1.setChecked(true);
			se.putInt(ts,0);
			se.commit();
		}
		
		if(sp.getInt(km,0)<=2 && !(sp.getInt(km,0)<0)){
			if(sp.getInt(km,0)==0){
				ab1.setEnabled(false);
				ab2.setEnabled(true);
				ab3.setEnabled(true);
				k = 0;
			}

			if(sp.getInt(km,0)==1){
				ab1.setEnabled(true);
				ab2.setEnabled(false);
				ab3.setEnabled(true);
				k = 1;
			}

			if(sp.getInt(km,0)==2){
				ab1.setEnabled(true);
				ab2.setEnabled(true);
				ab3.setEnabled(false);
				k = 2;
			}
			
		} else {
			se.putInt(km,0);
			se.commit();
			ab1.setEnabled(false);
			ab2.setEnabled(true);
			ab3.setEnabled(true);
			k = 0;
		}
		
		pb1.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View p)
			{
				try{
					panoyaKY(getBaseContext()," ",1,false);
					et1.setText(temp);
					et1.setSelection(et1.getText().toString().length());
					temp = "";
				} catch (Exception e){
					uyariGoster(1);
					hata = true;
				}
			}
		});
		
		pb2.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View p)
				{
					try{
						panoyaKY(getBaseContext()," ",1,false);
						temp = temp.replaceAll("\\D+","");
						et2.setText(temp);
						et2.setSelection(et2.getText().toString().length());
						temp = "";
					} catch (Exception e){
						uyariGoster(1);
						hata = true;
					}
				}
			});
		
		gms.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			public void onCheckedChanged(CompoundButton cb, boolean b){
				if(b){
					se.putBoolean(gm,true);
					geceModu(true);
				} else {
					se.putBoolean(gm,false);
					geceModu(false);
				}
				se.commit();
			}
		});
		
		dsy.setOnCheckedChangeListener(new OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton cb, boolean b){
					if(b) dsyz = true;
					else dsyz = false;
					se.putBoolean(dy,dsyz);
					se.commit();
				}
			});
		
		rms.setOnCheckedChangeListener(new OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton cb, boolean b){
					if(b) rand = true;
					else rand = false;
					se.putBoolean(rm,rand);
					se.commit();
				}
			});
			
		rns.setOnCheckedChangeListener(new OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton cb, boolean b){
					if(b) nrnd = true;
					else nrnd = false;
					eksiKDO(nrnd);
					se.putBoolean(re,nrnd);
					se.commit();
				}
			});
			
		ktc.setOnCheckedChangeListener(new OnCheckedChangeListener(){
				public void onCheckedChanged(CompoundButton cb, boolean b){
					se.putBoolean(tc,b);
					se.commit();
				}
			});
			
		rbs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
	            @Override
	            public void onCheckedChanged(RadioGroup rndm, int rnds) {
	                if(rnds == R.id.rnd1) se.putInt(rb,0);
	                if(rnds == R.id.rnd2) se.putInt(rb,1);
	                if(rnds == R.id.rnd3) se.putInt(rb,2);
	                if(rnds == R.id.rnd4) se.putInt(rb,3);
	                if(rnds == R.id.rnd5) se.putInt(rb,4);
	                if(rnds == R.id.rnd6) se.putInt(rb,5);
	                if(rnds == R.id.rnd7) se.putInt(rb,6);
	                if(rnds == R.id.rnd8) se.putInt(rb,7);
					se.commit();
	            }
	        });
			
		tms.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
	            @Override
	            public void onCheckedChanged(RadioGroup rndm, int rnds) {
	                if(rnds == R.id.tm1) {
	                    se.putInt(ts,0);
						temaDegistir(0,sp.getBoolean(gm,false));
	                } if(rnds == R.id.tm2) {
	                    se.putInt(ts,1);
						temaDegistir(1,sp.getBoolean(gm,false));
	                } if(rnds == R.id.tm3) {
	                    se.putInt(ts,2);
						temaDegistir(2,sp.getBoolean(gm,false));
	                } if(rnds == R.id.tm4) {
	                    se.putInt(ts,3);
						temaDegistir(3,sp.getBoolean(gm,false));
	                }
					se.commit();
	            }
	        });
		
		abt.setOnClickListener(new View.OnClickListener(){
			public void onClick(View uu){
				if(aym.isShown()){
					aym.setAlpha(1.0f);
					aym.animate()
						.translationY(0)
						.setDuration(300)
						.setStartDelay(0)
						.alpha(0.0f)
						.setListener(new AnimatorListenerAdapter() {
							@Override
							public void onAnimationEnd(Animator anim){
								aym.setVisibility(View.GONE);
								menuGiris(false);
								hkb.setEnabled(true);
								geri = false;
								abt.setText(getResources().getString(R.string.ayarmenua));
							}
						});
				} else {
					aym.setAlpha(0.0f);
					aym.animate()
						.translationY(0)
						.setDuration(300)
						.setStartDelay(0)
						.alpha(1.0f)
						.setListener(new AnimatorListenerAdapter() {
							@Override
							public void onAnimationStart(Animator anim){
								aym.setVisibility(View.VISIBLE);
								menuGiris(true);
								hkb.setEnabled(false);
								geri = true;
							}
							public void onAnimationEnd(Animator a){
								abt.setText(getResources().getString(R.string.geri_don));
							}
						});
				}
			}
		});
		
		ab1.setOnClickListener(new View.OnClickListener(){
				public void onClick(View x){
					se.putInt(km,0);
					se.commit();
					ab1.setEnabled(false);
					ab2.setEnabled(true);
					ab3.setEnabled(true);
					k = 0;
				}
			});

		ab2.setOnClickListener(new View.OnClickListener(){
				public void onClick(View x){
					se.putInt(km,1);
					se.commit();
					ab1.setEnabled(true);
					ab2.setEnabled(false);
					ab3.setEnabled(true);
					k = 1;
				}
			});

		ab3.setOnClickListener(new View.OnClickListener(){
				public void onClick(View x){
					se.putInt(km,2);
					se.commit();
					ab1.setEnabled(true);
					ab2.setEnabled(true);
					ab3.setEnabled(false);
					k = 2;
				}
			});
		
		sv1.setOnTouchListener(new View.OnTouchListener(){
			public boolean onTouch(View p1, MotionEvent p2){
				hideSoftKeyboard();
				unFocusEditText();
				return false;
			}
		});
		
		et1.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {}
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(et1.getText().toString().length()>0){
						bt1.setEnabled(true);
						bt3.setEnabled(true);
						btn1 = true;
						btn3 = true;
						sb1.setVisibility(View.VISIBLE);
					} else {
						bt1.setEnabled(false);
						if(et2.getText().toString().length()==0 & et3.getText().toString().length()==0){
							bt3.setEnabled(false);
							btn3 = false;
						}
						btn1 = false;
						sb1.setVisibility(View.INVISIBLE);
					}
				}
			});
		
		et2.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {}
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(et2.getText().toString().length()>0){
						bt2.setEnabled(true);
						bt3.setEnabled(true);
						btn2 = true;
						btn3 = true;
						sb2.setVisibility(View.VISIBLE);
					} else {
						bt2.setEnabled(false);
						if(et1.getText().toString().length()==0 & et3.getText().toString().length()==0){
							bt3.setEnabled(false);
							btn3 = false;
						}
						btn2 = false;
						sb2.setVisibility(View.INVISIBLE);
					}
				}
			});
			
		et3.addTextChangedListener(new TextWatcher() {
				public void afterTextChanged(Editable s) {}
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(et3.getText().toString().length()>0){
						bt3.setEnabled(true);
						btn3 = true;
						sb3.setVisibility(View.VISIBLE);
						if(et3.getText().toString().startsWith("-")) eksiKDO(true);
						else eksiKDO(false);
					} else {
						if(et1.getText().toString().length()==0 & et2.getText().toString().length()==0){
							bt3.setEnabled(false);
							btn3 = false;
						}
						sb3.setVisibility(View.INVISIBLE);
					}
				}
			});
		
		bt1.setOnClickListener(new View.OnClickListener(){
			public void onClick(View x){
				try{
					if(et3.length()==0){
						if(rand){
							eksiKDO(nrnd);
							et3.setText(random()+"");
						} else et3.setText("0");
						mKriptola();
					} else mKriptola();
				} catch(Exception u){
					uyariGoster(1);
					hata = true;
				}
			}
		});
		
		bt2.setOnClickListener(new View.OnClickListener(){
			public void onClick(View x){
				try{
					if(et3.length()==0){
						et3.setText("0");
						mCoz();
					} else mCoz();
				} catch(Exception v){
					uyariGoster(1);
					hata = true;
				}
			}
		});
		
		bt3.setOnClickListener(new View.OnClickListener(){
			public void onClick(View t){
				try{
					et1.setText("");
					et2.setText("");
					et3.setText("");
					unFocusEditText();
				} catch(Exception e){
					hata = false;
					uyariGoster(0);
				}
			}
		});
		
		bt4.setOnClickListener(new View.OnClickListener(){
			public void onClick(View q){
				try{
					if(sp.getInt(km,0)<=2 & !(sp.getInt(km,0)<0)){
						if(sp.getInt(km,0)==0){
							ab1.setEnabled(false);
							ab2.setEnabled(true);
							ab3.setEnabled(true);
							k = 0;
							if(et2.getText().toString().replaceAll("\\D+","").length()>0) 
								panoyaKY(getBaseContext(),metinCek(0),1,true);
							else panoyaKY(getBaseContext()," ",4,true);
						}

						if(sp.getInt(km,0)==1){
							ab1.setEnabled(true);
							ab2.setEnabled(false);
							ab3.setEnabled(true);
							k = 1;
							if(et2.getText().toString().replaceAll("\\D+","").length()>0 && et3.getText().toString().length()>0){
								panoyaKY(getBaseContext(),metinCek(1),2,true);
							} else {
								panoyaKY(getBaseContext()," ",4,true);
							}
						}

						if(sp.getInt(km,0)==2){
							ab1.setEnabled(true);
							ab2.setEnabled(true);
							ab3.setEnabled(false);
							k = 2;
							if(et1.getText().toString().length()>0 && et2.getText().toString().replaceAll("\\D+","").length()>0 && et3.getText().toString().length()>0)
								panoyaKY(getBaseContext(),metinCek(2),3,true);
							else panoyaKY(getBaseContext()," ",4,true);
						}

					} else {
						se.putInt(km,0);
						se.commit();
						k = 0;
						if(et2.getText().toString().replaceAll("\\D+","").length()>0) 
							panoyaKY(getBaseContext(),et2.getText().toString(),1,true);
						else panoyaKY(getBaseContext()," ",4,true);
					}
				} catch(Exception m){
					hata = true;
					uyariGoster(4);
				}
			}
		});
		
		sb1.setOnClickListener(new View.OnClickListener(){
			public void onClick(View x){ unFocusET(0); }
		});

		sb2.setOnClickListener(new View.OnClickListener(){
			public void onClick(View x){ unFocusET(1); }
		});
		
		sb3.setOnClickListener(new View.OnClickListener(){
			public void onClick(View x){ unFocusET(2); }
		});
		
		rnd.setOnClickListener(new View.OnClickListener(){
			public void onClick(View x){
				et3.setText(random()+"");
				et3.setSelection(et3.getText().toString().length());
			}
		});
		
		hkb.setOnClickListener(new View.OnClickListener(){
			public void onClick(View x){
				if(hky.isShown()){
					hky.setAlpha(1.0f);
					hky.animate()
						.translationY(0)
						.setDuration(300)
						.setStartDelay(0)
						.alpha(0.0f)
						.setListener(new AnimatorListenerAdapter() {
							@Override
							public void onAnimationEnd(Animator anim){
								hky.setVisibility(View.GONE);
								menuGiris(false);
								abt.setEnabled(true);
								geri = false;
								hkb.setText(getResources().getString(R.string.hkkbmetin));
								hkm.setText(getResources().getString(R.string.hkkametin));
							}
						});
				} else {
					hky.setAlpha(0.0f);
					hky.animate()
						.translationY(0)
						.setDuration(300)
						.setStartDelay(0)
						.alpha(1.0f)
						.setListener(new AnimatorListenerAdapter() {
							@Override
							public void onAnimationStart(Animator anim){
								hky.setVisibility(View.VISIBLE);
								menuGiris(true);
								abt.setEnabled(false);
								geri = true;
							}
							public void onAnimationEnd(Animator a){
								hkb.setText(getResources().getString(R.string.geri_don));
							}
						});
				}
			}
		});
    }
	
	public void mKriptola(){
		et2.setText(krhex(kriptola2(kriptola1())));
		et2.setSelection(et2.getText().toString().length());
		if(dsyz) dosyayaYaz(true,k);
	}
	
	public void mCoz(){
		et1.setText(coz3(coz2(hexts())));
		et1.setSelection(et1.getText().toString().length());
		if(dsyz) dosyayaYaz(false,k);
	}
	
	public String krhex(String h){
		char[] chars = h.toCharArray();
		StringBuffer hex = new StringBuffer();
		for(char ch : chars) hex.append(Integer.toHexString((int)ch)+"");
		return hex.toString().trim();
	}
	
	public String hexts(){
		String hex = et2.getText().toString().trim().replaceAll("\\D+","");
		if(TextUtils.isDigitsOnly(hex)){
			try {
				StringBuilder str = new StringBuilder();
				for (int i = 0; i < hex.length(); i+=2) str.append((char)Integer.parseInt(hex.substring(i, i + 2), 16));
				return str.toString();
			} catch (Exception x){
				uyariGoster(1);
				hata = true;
				return "";
			}
		} else {
			uyariGoster(1);
			hata = true;
			return "";
		}
		
	}
	
	public String kriptola1(){
		String str = et1.getText().toString();
		StringBuilder sb = new StringBuilder();
		Integer kdo = new Integer(Integer.parseInt(et3.getText().toString()));
		for (char c : str.toCharArray()){
				int p = c - kdo;
				sb.append(p+" ");
		}
		return sb.toString().trim();
	}
	
	public String kriptola2(String s){
		StringBuilder su = new StringBuilder();
		for (char c : s.toCharArray()) su.append((int)c+" ");
		if(sp.getBoolean(tc,false))
			return su.reverse().toString().trim();
		else return su.toString().trim();
	}
	
	public String coz2(String s){
		try{
			StringBuffer zz = new StringBuffer();
			String e = "";
			if(sp.getBoolean(tc,false))
				e = new StringBuilder().append(s.trim()).reverse().toString();
			else e = s.trim();
			String[] yyy = e.split(" ");
			for(int yy = 0;yy < yyy.length;yy++)
					zz.append(Character.toString((char)Integer.parseInt(yyy[yy])));
			return zz.toString();
		}catch (Exception e){
			uyariGoster(1);
			hata = true;
			return "";
		}
	}
	
	public String coz3(String s){
		try{
			StringBuffer zz = new StringBuffer();
			String e = s.trim();
			String[] yyy = e.split(" ");
			String xxx = new String();
			Integer intt = new Integer(0);
			Integer kdo = new Integer(Integer.parseInt(et3.getText().toString()));
			for(int yy = 0 ; yy < yyy.length ; yy++){
				intt = Integer.parseInt(yyy[yy]);
				int uu = intt + kdo;
				xxx = Character.toString((char)uu);
				zz.append(xxx);
			}
			return zz.toString();
		}catch (Exception e){
			Log.e(getResources().getString(R.string.app_name),getResources().getString(R.string.hata_fix));
			return "";
		}

	}
	
	public void hideSoftKeyboard() {
			((InputMethodManager) getSystemService(
            Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
			getCurrentFocus().getWindowToken(), 0);
	}
	
	public void unFocusEditText(){
		et1.setFocusable(false);
		et2.setFocusable(false);
		et3.setFocusable(false);
		et1.setFocusableInTouchMode(true);
		et2.setFocusableInTouchMode(true);
		et3.setFocusableInTouchMode(true);
	}
	
	public void sdk(){
		View sb = findViewById(R.id.sb);
		if(Build.VERSION.SDK_INT==19) sb.setVisibility(View.VISIBLE);
		else sb.setVisibility(View.GONE);
	}
	
	public void uyariGoster(final int kt){
		hky.setAlpha(0.0f);
		hky.animate()
			.translationY(0)
			.setDuration(300)
			.setStartDelay(0)
			.alpha(1.0f)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationStart(Animator anim){
					hky.setVisibility(View.VISIBLE);
					menuGiris(true);
					hkb.setEnabled(false);
					abt.setEnabled(false);
					if(!ucik) geri = true;
					hkb.setText(getResources().getString(R.string.hkkbmetin));
					if(hata){
						if(kt==4) hkm.setText(getResources().getString(R.string.kopylhata));
						else hkm.setText(getResources().getString(R.string.hata_fix));
					} else {
						String k = new String();
						if(kt==0) hkm.setText(getResources().getString(R.string.ucikisyap));
						else {
							if(kt==1) k=getResources().getString(R.string.kopymodua);
							if(kt==2) k=getResources().getString(R.string.ousfmetin)+getResources().getString(R.string.veorankpy);
							if(kt==3) k=getResources().getString(R.string.kopymoduc);
							if(!(kt==0)&&!(kt==1)&&!(kt==2)&&!(kt==3)) k="0 ";
							hkm.setText(k+getResources().getString(R.string.panokopyl));
						}
					}
				}

				public void onAnimationEnd(Animator a){
					hky.setAlpha(1.0f);
					hky.animate()
						.translationY(0)
						.setStartDelay(5000)
						.setDuration(300)
						.alpha(0.0f)
						.setListener(new AnimatorListenerAdapter() {
							@Override
							public void onAnimationEnd(Animator anim){
								hky.setVisibility(View.GONE);
								menuGiris(false);
								hkb.setEnabled(true);
								abt.setEnabled(true);
								geri = false;
								hkb.setText(getResources().getString(R.string.hkkbmetin));
								hkm.setText(getResources().getString(R.string.hkkametin));
							}
						});
				}

			});
		}
	
	public String temp = new String();
		
	private void panoyaKY(Context context, String ky,int kt,boolean b) {
		if(kt<=3 && kt>=0){
			if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
				android.text.ClipboardManager pano = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
				if(b) pano.setText(ky);
				else temp = pano.getText().toString();
			} else {
				android.content.ClipboardManager pano = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
				android.content.ClipData clip = android.content.ClipData.newPlainText(getResources().getString(R.string.app_name), ky);
				if(b) pano.setPrimaryClip(clip);
				else temp = pano.getText().toString();
			}
			if(b){
				hata = false;
				uyariGoster(kt);
			}
		} else {
			hata = true;
			uyariGoster(kt);
		}
	}
	
	public int random(){
		if(sp.getInt(rb,0)>=0 && sp.getInt(rb,0)<8){
			int x = 0;
			StringBuilder sb = new StringBuilder();
			while(true){
				if(x == sp.getInt(rb,0)){
					break;
				} else {
					sb.append(0+"");
					x++;
				}
			}
			Integer i = (new Random()).nextInt(Integer.parseInt("9"+sb.toString()))+Integer.parseInt("1"+sb.toString());
			if(nrnd && i != 0) return -i;
			else return i;
		} else return 0;
		
	}
	
	@Override
	public void onBackPressed(){
		if(geri){
			hky.setAlpha(1.0f);
			hky.animate()
				.translationY(0)
				.setStartDelay(0)
				.setDuration(300)
				.alpha(0.0f)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator anim){
						hky.setVisibility(View.GONE);
						hkb.setText(getResources().getString(R.string.hkkbmetin));
						hkm.setText(getResources().getString(R.string.hkkametin));
					}
				});
				
			aym.setAlpha(1.0f);
			aym.animate()
				.translationY(0)
				.setDuration(300)
				.setStartDelay(0)
				.alpha(0.0f)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator anim){
						aym.setVisibility(View.GONE);
						abt.setText(getResources().getString(R.string.ayarmenua));
					}
				});
			geri = false;
			menuGiris(geri);
			abt.setEnabled(true);
			hkb.setEnabled(true);
		} else {
			if(ucik){
				super.onBackPressed();
				return;
			}
			ucik = true;
			hata = false;
			uyariGoster(0);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					ucik=false;                       
				}
			}, 2000);
		} 
	}
	
	public void eksiKDO(boolean b){
		if(b) et3.setFilters(new InputFilter[] { new InputFilter.LengthFilter(9) });
		else et3.setFilters(new InputFilter[] { new InputFilter.LengthFilter(8) });
	}
	
	public void geceModu(boolean b){
		if(b){
			et1.setTextColor(getResources().getColor(android.R.color.white));
			et2.setTextColor(getResources().getColor(android.R.color.white));
			et3.setTextColor(getResources().getColor(android.R.color.white));
			bt1.setTextColor(getResources().getColor(android.R.color.white));
			bt2.setTextColor(getResources().getColor(android.R.color.white));
			bt3.setTextColor(getResources().getColor(android.R.color.white));
			bt4.setTextColor(getResources().getColor(android.R.color.white));
			ab1.setTextColor(getResources().getColor(android.R.color.white));
			ab2.setTextColor(getResources().getColor(android.R.color.white));
			ab3.setTextColor(getResources().getColor(android.R.color.white));
			pb1.setTextColor(getResources().getColor(android.R.color.white));
			pb2.setTextColor(getResources().getColor(android.R.color.white));
			sb1.setTextColor(getResources().getColor(android.R.color.white));
			sb2.setTextColor(getResources().getColor(android.R.color.white));
			sb3.setTextColor(getResources().getColor(android.R.color.white));
			dsy.setTextColor(getResources().getColor(android.R.color.white));
			gms.setTextColor(getResources().getColor(android.R.color.white));
			rms.setTextColor(getResources().getColor(android.R.color.white));
			rns.setTextColor(getResources().getColor(android.R.color.white));
			rnd.setTextColor(getResources().getColor(android.R.color.white));
			hkm.setTextColor(getResources().getColor(android.R.color.white));
			kms.setTextColor(getResources().getColor(android.R.color.white));
			rba.setTextColor(getResources().getColor(android.R.color.white));
			ktc.setTextColor(getResources().getColor(android.R.color.white));
			tet1.setTextColor(getResources().getColor(android.R.color.white));
			tet2.setTextColor(getResources().getColor(android.R.color.white));
			tet3.setTextColor(getResources().getColor(android.R.color.white));
			rnd1.setTextColor(getResources().getColor(android.R.color.white));
			rnd2.setTextColor(getResources().getColor(android.R.color.white));
			rnd3.setTextColor(getResources().getColor(android.R.color.white));
			rnd4.setTextColor(getResources().getColor(android.R.color.white));
			rnd5.setTextColor(getResources().getColor(android.R.color.white));
			rnd6.setTextColor(getResources().getColor(android.R.color.white));
			rnd7.setTextColor(getResources().getColor(android.R.color.white));
			rnd8.setTextColor(getResources().getColor(android.R.color.white));
			tsc.setTextColor(getResources().getColor(android.R.color.white));
			tm1.setTextColor(getResources().getColor(android.R.color.white));
			tm2.setTextColor(getResources().getColor(android.R.color.white));
			tm3.setTextColor(getResources().getColor(android.R.color.white));
			tm4.setTextColor(getResources().getColor(android.R.color.white));
			aym.setBackgroundColor(getResources().getColor(R.color.arkaplan));
			hky.setBackgroundColor(getResources().getColor(R.color.arkaplan));
			sv1.setBackgroundColor(getResources().getColor(R.color.arkaplan));
		} else {
			et1.setTextColor(getResources().getColor(android.R.color.black));
			et2.setTextColor(getResources().getColor(android.R.color.black));
			et3.setTextColor(getResources().getColor(android.R.color.black));
			bt1.setTextColor(getResources().getColor(android.R.color.black));
			bt2.setTextColor(getResources().getColor(android.R.color.black));
			bt3.setTextColor(getResources().getColor(android.R.color.black));
			bt4.setTextColor(getResources().getColor(android.R.color.black));
			ab1.setTextColor(getResources().getColor(android.R.color.black));
			ab2.setTextColor(getResources().getColor(android.R.color.black));
			ab3.setTextColor(getResources().getColor(android.R.color.black));
			aym.setBackgroundColor(getResources().getColor(android.R.color.white));
			hky.setBackgroundColor(getResources().getColor(android.R.color.white));
			sv1.setBackgroundColor(getResources().getColor(android.R.color.white));
			temaDegistir(sp.getInt(ts,0),false);
		}
	}
	
	public void menuGiris(boolean b){
		if(b){
			et1.setEnabled(false);
			et2.setEnabled(false);
			et3.setEnabled(false);
			sb1.setEnabled(false);
			sb2.setEnabled(false);
			sb3.setEnabled(false);
			bt1.setEnabled(false);
			bt2.setEnabled(false);
			bt3.setEnabled(false);
			bt4.setEnabled(false);
		} else {
			et1.setEnabled(true);
			et2.setEnabled(true);
			et3.setEnabled(true);
			sb1.setEnabled(true);
			sb2.setEnabled(true);
			sb3.setEnabled(true);
			bt1.setEnabled(btn1);
			bt2.setEnabled(btn2);
			bt3.setEnabled(btn3);
			bt4.setEnabled(true);
		}
	}
	
	public void dosyayaYaz(boolean x, int i){
		try {
			Calendar takvim = Calendar.getInstance();
			int a = takvim.get(Calendar.DAY_OF_MONTH);
			int b = takvim.get(Calendar.MONTH);
			int c = takvim.get(Calendar.YEAR);
			int d = takvim.get(Calendar.HOUR_OF_DAY);
			int e = takvim.get(Calendar.MINUTE);
			int f = takvim.get(Calendar.SECOND);
			String y = new String();
			String z = (getResources().getString(R.string.app_name)).replace(" ","");
			if(x){
				y = "encrypted";
				if(i == 0) temp = metinCek(0);
				if(i == 1) temp = metinCek(1);
				if(i == 2) temp = metinCek(2);
				if(i < 0 || i > 2) temp = metinCek(4);
			} else {
				y = "decrypted";
				if(i == 0) temp = metinCek(3);
				if(i == 1 || i == 2) temp = metinCek(2);
				if(i < 0 || i > 2) temp = metinCek(4);
			}
			File root = new File(Environment.getExternalStorageDirectory()+"/"+z);
			if(!root.exists()) root.mkdir();
			FileWriter writer = new FileWriter(new File(root, z+"-"+sayiDuzeltici(a)+"."+sayiDuzeltici(b)+"."+sayiDuzeltici(c)+"-"+sayiDuzeltici(d)+":"+sayiDuzeltici(e)+":"+sayiDuzeltici(f)+"-"+y+".txt"));
			writer.append(temp);
			writer.flush();
			writer.close();
			temp = "";
		} catch (Exception e) { Log.e("WTinTXT",e.getMessage()); }
	}
	
	public String metinCek(int i){
		if (i == 0) return et2.getText().toString().replaceAll("\\D+","");
		if (i == 1) return getResources().getString(R.string.ousfmetin)+":\n"+et2.getText().toString().replaceAll("\\D+","")+"\n\n"+getResources().getString(R.string.kdormetin)+": "+et3.getText().toString();
		if (i == 2) return getResources().getString(R.string.insfmetin)+":\n"+et1.getText().toString()+"\n\n"+getResources().getString(R.string.ousfmetin)+":\n"+et2.getText().toString().replaceAll("\\D+","")+"\n\n"+getResources().getString(R.string.kdormetin)+": "+et3.getText().toString();
		if (i == 3) return et1.getText().toString();
		return "FAILED!";
	}
	
	public String sayiDuzeltici(int i){
		if(i <= 9) return "0"+i;
		else return ""+i;
	}
	
	public void temaDegistir(int i, boolean b){
		if(!b){
			if(i == 0) renklendir1(getResources().getColor(R.color.sanRenk1));
			if(i == 1) renklendir1(getResources().getColor(R.color.sanRenk2));
			if(i == 2) renklendir1(getResources().getColor(R.color.sanRenk3));
			if(i == 3) renklendir1(getResources().getColor(R.color.sanRenk4));
			tm1.setTextColor(getResources().getColor(R.color.sanRenk1));
			tm2.setTextColor(getResources().getColor(R.color.sanRenk2));
			tm3.setTextColor(getResources().getColor(R.color.sanRenk3));
			tm4.setTextColor(getResources().getColor(R.color.sanRenk4));
		}
		
		if(i == 0) renklendir2(R.drawable.radbt, R.drawable.btn,
					    R.drawable.btna, R.drawable.mkutu,
					    getResources().getColor(R.color.anaRenk1),
					    getResources().getColor(R.color.sanRenk1));
		if(i == 1) renklendir2(R.drawable.radbt2, R.drawable.btn_2,
					    R.drawable.btnb, R.drawable.mkutu_2,
					    getResources().getColor(R.color.trnRenk1),
					    getResources().getColor(R.color.sanRenk2));
		if(i == 2) renklendir2(R.drawable.radbt3, R.drawable.btn_3,
					    R.drawable.btnc, R.drawable.mkutu_3,
					    getResources().getColor(R.color.yslRenk1),
					    getResources().getColor(R.color.sanRenk3));
		if(i == 3) renklendir2(R.drawable.radbt4, R.drawable.btn_4,
						R.drawable.btnd, R.drawable.mkutu_4,
						getResources().getColor(R.color.pmbRenk1),
						getResources().getColor(R.color.sanRenk4));
	}
	
	public void renklendir1(int c){
		pb1.setTextColor(c);
		pb2.setTextColor(c);
		sb1.setTextColor(c);
		sb2.setTextColor(c);
		sb3.setTextColor(c);
		dsy.setTextColor(c);
		gms.setTextColor(c);
		rms.setTextColor(c);
		rns.setTextColor(c);
		rba.setTextColor(c);
		rnd.setTextColor(c);
		hkm.setTextColor(c);
		kms.setTextColor(c);
		tsc.setTextColor(c);
		ktc.setTextColor(c);
		tet1.setTextColor(c);
		tet2.setTextColor(c);
		tet3.setTextColor(c);
		rnd1.setTextColor(c);
		rnd2.setTextColor(c);
		rnd3.setTextColor(c);
		rnd4.setTextColor(c);
		rnd5.setTextColor(c);
		rnd6.setTextColor(c);
		rnd7.setTextColor(c);
		rnd8.setTextColor(c);
	}
	
	public void renklendir2(int d1, int d2, int d3, int d4, int i1,int i2){
		sb.setBackgroundColor(i1);
		tb.setBackgroundColor(i1);
		rnd1.setButtonDrawable(d1);
		rnd2.setButtonDrawable(d1);
		rnd3.setButtonDrawable(d1);
		rnd4.setButtonDrawable(d1);
		rnd5.setButtonDrawable(d1);
		rnd6.setButtonDrawable(d1);
		rnd7.setButtonDrawable(d1);
		rnd8.setButtonDrawable(d1);
		bt1.setBackgroundDrawable(getResources().getDrawable(d2));
		bt2.setBackgroundDrawable(getResources().getDrawable(d2));
		bt3.setBackgroundDrawable(getResources().getDrawable(d2));
		bt4.setBackgroundDrawable(getResources().getDrawable(d2));
		ab1.setBackgroundDrawable(getResources().getDrawable(d3));
		ab2.setBackgroundDrawable(getResources().getDrawable(d3));
		ab3.setBackgroundDrawable(getResources().getDrawable(d3));
		et1.setBackgroundDrawable(getResources().getDrawable(d4));
		et2.setBackgroundDrawable(getResources().getDrawable(d4));
		et3.setBackgroundDrawable(getResources().getDrawable(d4));
		if(Build.VERSION.SDK_INT>=21){
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			getWindow().setStatusBarColor(i2);
			getWindow().setNavigationBarColor(i2);
		}
	}
	
	public void unFocusET(int i){
		if(i == 0){
			et1.setText("");
			et1.setFocusable(false);
			et1.setFocusableInTouchMode(true);
		} if(i == 1){
			et2.setText("");
			et2.setFocusable(false);
			et2.setFocusableInTouchMode(true);
		} if(i == 2){
			et3.setText("");
			et3.setFocusable(false);
			et3.setFocusableInTouchMode(true);
		}
		hideSoftKeyboard();
	}
}
